import { createApp } from 'vue'
import App from './App.vue'
import { initRouter } from './router/router'
import { initStore } from './store/store'
import './assets/base.less'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

const app = createApp(App)

//注册所有图标
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}

initRouter(app)
initStore(app)
app.mount('#app')
